import 'package:flutter/material.dart';

class FirstScreen extends StatelessWidget {
  Widget build(BuildContext context) {
    return (MaterialApp(
      title: "First screen",
      home: Material(
        color: Colors.white12,
        child: Center(
          child: Text(
            "first app",
            style: TextStyle(color: Colors.blue),
          ),
        ),
      ),
    ));
  }
}
