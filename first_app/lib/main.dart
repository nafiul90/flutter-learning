import 'package:flutter/material.dart';
import './app_screens/first_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  Widget build(BuildContext context) {
    return (MaterialApp(
        title: "My material App",
        home: Scaffold(
            appBar: AppBar(
              title: Text("My first App"),
            ),
            body: FirstScreen())));
  }
}
