import 'package:flutter/material.dart';

class FlushAnimation extends StatefulWidget {
  @override
  _FlushAnimationState createState() => _FlushAnimationState();
}

class _FlushAnimationState extends State<FlushAnimation> {
  double _width = 200;
  double _height = 200;

  double _updateState() {
    setState(() {
      _width = 300;
      _height = 300;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          RaisedButton(
            onPressed: _updateState,
            child: Text('Animate'),
          ),
          Center(
            child: AnimatedContainer(
              duration: Duration(milliseconds: 900),
              width: 200,
              height: _height,
              curve: Curves.bounceOut,
              color: Colors.lightBlue[200],
              child: Center(
                child: Text('Animation'),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
