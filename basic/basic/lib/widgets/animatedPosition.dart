import 'package:flutter/material.dart';

class SideDrawer extends StatefulWidget {
  @override
  _SideDrawerState createState() => _SideDrawerState();
}

class _SideDrawerState extends State<SideDrawer> {
  double _right = 300;

  void _updateState() {
    setState(() {
      _right = 10;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        AnimatedPositioned(
          duration: Duration(milliseconds: 500),
          right: _right,
          height: double.infinity,
          width: 300,
          child: RaisedButton(
            child: Text('tap here'),
            onPressed: _updateState,
          ),
        ),
      ],
    );
  }
}
