import 'package:flutter/material.dart';

class ProductList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final productList = [
      {
        'name': 'shirt',
        'imagePath': '',
        'price': 500,
        'description': 'nice shirt'
      },
      {
        'name': 'shirt 2',
        'imagePath': '',
        'price': 500,
        'description': 'nice shirt2'
      },
      {
        'name': 'shirt 3',
        'imagePath': '',
        'price': 500,
        'description': 'nice shirt3'
      },
      {
        'name': 'shirt',
        'imagePath': '',
        'price': 500,
        'description': 'nice shirt'
      },
      {
        'name': 'shirt 2',
        'imagePath': '',
        'price': 500,
        'description': 'nice shirt2'
      },
      {
        'name': 'shirt 3',
        'imagePath': '',
        'price': 500,
        'description': 'nice shirt3'
      },
      {
        'name': 'shirt',
        'imagePath': '',
        'price': 500,
        'description': 'nice shirt'
      },
      {
        'name': 'shirt 2',
        'imagePath': '',
        'price': 500,
        'description': 'nice shirt2'
      },
      {
        'name': 'shirt 3',
        'imagePath': '',
        'price': 500,
        'description': 'nice shirt3'
      },
    ];

    return Container(
      padding: EdgeInsets.only(top: 50),
      child: ListView(
        children: productList.map((e) {
          return Padding(
            padding: const EdgeInsets.only(left: 20.0, right: 20.0),
            child: Card(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(children: [
                  Text('image'),
                  Expanded(
                    child: Container(
                      padding: EdgeInsets.only(left: 30),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(e['name']),
                          Text(e['description']),
                          Text(e['price'].toString())
                        ],
                      ),
                    ),
                  ),
                  IconButton(
                    alignment: Alignment.centerRight,
                    onPressed: () {},
                    icon: Icon(Icons.add),
                  ),
                ]),
              ),
            ),
          );
        }).toList(),
      ),
    );
  }
}
