import 'dart:async';

import 'package:flutter/material.dart';

class SideBar extends StatefulWidget {
  @override
  _SideBarState createState() => _SideBarState();
}

class _SideBarState extends State<SideBar>
    with SingleTickerProviderStateMixin<SideBar> {
  // StreamController<bool> isSideBarOpenedStreamController;
  // Stream<bool> isSideBarOpenedStream;
  StreamSink<bool> isSideBarOpenedSink;
  AnimationController _animationController;

  bool isSideBarOpened = false;
  final _animationDuration = const Duration(milliseconds: 300);
  Animation animation;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _animationController =
        AnimationController(vsync: this, duration: _animationDuration);
    // isSideBarOpenedStreamController = PublishSubject<bool>();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    // isSideBarOpenedStreamController.close();
    isSideBarOpenedSink.close();
    _animationController.dispose();
    super.dispose();
  }

  void onOpenSideBar() {
    setState(() {
      isSideBarOpened = !isSideBarOpened;
      isSideBarOpened
          ? _animationController.forward()
          : _animationController.reverse();
    });
  }

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;

    return AnimatedPositioned(
      duration: _animationDuration,
      top: 0,
      bottom: 0,
      left: isSideBarOpened ? 0 : 0,
      right: isSideBarOpened ? 0 : screenWidth - 45,
      child: Row(children: [
        Expanded(
          child: Container(
            color: Color(0xFF262AAA),
          ),
        ),
        Align(
          alignment: Alignment(0, -0.9),
          child: GestureDetector(
            onTap: onOpenSideBar,
            child: ClipPath(
              clipper: new MyClipper(),
              child: Container(
                width: 35,
                height: 110,
                color: Color(0xFF262AAA),
                alignment: Alignment.centerLeft,
                child: AnimatedIcon(
                  progress: _animationController,
                  icon: AnimatedIcons.menu_close,
                  color: Color(0xFF1BB5FD),
                  size: 25,
                ),
              ),
            ),
          ),
        )
      ]),
    );
  }
}

class MyClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = new Path();
    path.quadraticBezierTo((size.width / 2) + 9, (size.height / 4) - 5,
        size.width - 7, size.height / 2);

    path.quadraticBezierTo((size.width / 2) + 9,
        (size.height) - (size.height / 4) + 5, 0, size.height);
    // path.lineTo(0, size.height);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}
