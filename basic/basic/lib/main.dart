import 'package:basic/widgets/animatedContainer.dart';
import 'package:basic/widgets/animatedPosition.dart';
import 'package:basic/widgets/animation.dart';
import 'package:basic/widgets/form.dart';
import 'package:basic/widgets/productList.dart';
import 'package:basic/widgets/sideBar/sideBar_layout.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
          scaffoldBackgroundColor: Colors.white, primaryColor: Colors.white),
      title: "basic App",
      home: SideBarLayout(),
    );
  }
}

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final appBar = AppBar(
      backgroundColor: Colors.yellowAccent,
      elevation: 0,
      title: Text('Basic app'),
      actions: [Icon(Icons.search)],
    );

    return Scaffold(
      backgroundColor: Colors.yellowAccent,
      appBar: appBar,
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              color: Colors.yellowAccent,
              height: 180,
              width: double.infinity,
            ),
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(topLeft: Radius.circular(50)),
                color: Colors.cyan,
              ),
              height: (MediaQuery.of(context).size.height -
                  MediaQuery.of(context).padding.top -
                  180 -
                  appBar.preferredSize.height),
              child: ProductList(),
            ),
            // FlushAnimation(),
            // SideDrawer(),
          ],
        ),
      ),
    );
  }
}
