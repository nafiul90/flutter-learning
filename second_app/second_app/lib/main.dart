import 'package:flutter/material.dart';
import './quez.dart';
import './result.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _MyAppState();
  }
}

class _MyAppState extends State<MyApp> {
  var questionIndex = 0;
  final questions = const [
    {
      'questionText': 'what is your favorite color?',
      'answares': ['black', 'red', 'green', 'blue']
    },
    {
      'questionText': 'what is your favorite animal?',
      'answares': ['rabbit', 'cat', 'cow', 'dog']
    }
  ];

  void _answareQuestion() {
    if (questionIndex < questions.length) {
      setState(() {
        questionIndex = questionIndex + 1;
      });
    }

    print('pressed');
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
            appBar: AppBar(
              title: Text('My first app'),
            ),
            body: questionIndex < questions.length
                ? Quiz(
                    answareQuestion: _answareQuestion,
                    questionIndex: questionIndex,
                    questions: questions,
                  )
                : Result()));
  }
}
