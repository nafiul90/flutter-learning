import 'package:flutter/material.dart';

class Answare extends StatelessWidget {
  final Function selecctHandler;
  final String answareText;

  Answare(this.selecctHandler, this.answareText);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: RaisedButton(
        color: Colors.blue,
        textColor: Colors.white,
        child: Text(answareText),
        onPressed: selecctHandler,
      ),
    );
  }
}
