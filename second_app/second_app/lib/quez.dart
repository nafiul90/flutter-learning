import 'package:flutter/material.dart';
import './answare.dart';
import './question.dart';

class Quiz extends StatelessWidget {
  final List<Map<String, Object>> questions;
  final int questionIndex;
  final Function answareQuestion;

  Quiz(
      {@required this.questions,
      @required this.answareQuestion,
      @required this.questionIndex});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Question(questions[questionIndex]['questionText']),
        ...(questions[questionIndex]['answares'] as List<String>)
            .map((answare) {
          return Answare(answareQuestion, answare);
        }).toList()
      ],
    );
  }
}
